<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class APIHelper
{
    public static function getLevels()
    {
        // Dummy data
        // return [
        //     "data" => [
        //         [
        //             "levelId" => 1,
        //             "levelName" => "Master Chef"
        //         ],
        //         [
        //             "levelId" => 2,
        //             "levelName" => "Hard"
        //         ]
        //     ]
        // ];

        // Actual Call API
        try {
            $response = Http::get('http://localhost:8080/api/book-recipe-masters/level-option-lists');
            return $response->json();
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function getCategories()
    {
        // return [
        //     "data" => [
        //         [
        //             "categoryId" => 1,
        //             "categoryName" => "Lunch"
        //         ],
        //         [
        //             "categoryId" => 2,
        //             "categoryName" => "Breakfast"
        //         ],
        //     ]
        // ];

        try {
            $response = Http::get('http://localhost:8080/api/book-recipe-masters/category-option-lists');
            return $response->json();
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function getRecipes($recipeName = null, $levelId = null, $categoryId = null, $time = null, $sortBy = null, $pageSize = 8, $pageNumber = 1)
    {
        // return [
        //     "total" => 2,
        //     "data" => [
        //         [
        //             "recipeId" => 1,
        //             "categories" => [
        //                 "categoryId" => 2,
        //                 "categoryName" => "Breakfast"
        //             ],
        //             "levels" => [
        //                 "levelId" => 1,
        //                 "levelName" => "Master Chef"
        //             ],
        //             "recipeName" => "Ayam Geprek",
        //             "imageFileName" => "https://www.masakapahariini.com/wp-content/uploads/2023/03/shutterstock_1949306203-500x300.jpg",
        //             "timeCook" => 3
        //         ],
        //         [
        //             "recipeId" => 23,
        //             "categories" => [
        //                 "categoryId" => 1,
        //                 "categoryName" => "Lunch"
        //             ],
        //             "levels" => [
        //                 "levelId" => 3,
        //                 "levelName" => "Medium"
        //             ],
        //             "recipeName" => "Bubur Ayam",
        //             "imageFileName" => "https://awsimages.detik.net.id/community/media/visual/2022/11/12/resep-bubur-ayam-claypot_43.jpeg?w=600&q=90",
        //             "timeCook" => 120
        //         ]
        //     ]

        // ];

        try {
            $endpoint = 'http://localhost:8080/api/book-recipes' . '?' . http_build_query([
                'recipeName' => $recipeName,
                'timeCook' => $time,
                'levelId' => $levelId,
                'categoryId' => $categoryId,
                'sortBy' => $sortBy,
                'pageSize' => $pageSize,
                'pageNumber' => $pageNumber
            ]);

            $response = Http::get($endpoint);
            return $response->json();
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function getRecipesDetail($recipeId)
    {
        try {
            $endpoint = 'localhost:8080/api/book-recipes' . '/' . $recipeId;
            $response = Http::get($endpoint);
            return $response->json();
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function updateRecipe($recipeId, $recipe)
    {
        try {
            $endpoint = 'http://localhost:8080/api/book-recipes/' . $recipeId;
            $response = Http::put($endpoint, $recipe);

            if ($response->successful()) {
                return $response->json();
            } else {
                $statusCode = $response->status();
                $errorMessage = $response->body();
                Log::error("Failed to update recipe. Status Code: $statusCode, Error Message: $errorMessage");
                throw new \Exception("Failed to update recipe. Status Code: $statusCode");
            }
        } catch (\Illuminate\Http\Client\RequestException $error) {
            Log::error($error->getMessage());
            throw $error;
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function createRecipe($data)
    {
        try {
            $endpoint = 'http://localhost:8080/api/book-recipes';
            $response = Http::post($endpoint, $data);

            if ($response->successful()) {
                return $response->json();
            } else {
                $statusCode = $response->status();
                $errorMessage = $response->body();
                Log::error("Failed to update recipe. Status Code: $statusCode, Error Message: $errorMessage");
                throw new \Exception("Failed to update recipe. Status Code: $statusCode");
            }
        } catch (\Illuminate\Http\Client\RequestException $error) {
            Log::error($error->getMessage());
            throw $error;
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }

    public static function deleteRecipe($recipeId)
    {
        try {
            $endpoint = 'http://localhost:8080/api/book-recipes/' . $recipeId;
            $response = Http::delete($endpoint);

            if ($response->successful()) {
                return $response->json();
            } else {
                $statusCode = $response->status();
                $errorMessage = $response->body();
                Log::error("Failed to update recipe. Status Code: $statusCode, Error Message: $errorMessage");
                throw new \Exception("Failed to update recipe. Status Code: $statusCode");
            }
        } catch (\Illuminate\Http\Client\RequestException $error) {
            Log::error($error->getMessage());
            throw $error;
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            throw $error;
        }
    }
}
