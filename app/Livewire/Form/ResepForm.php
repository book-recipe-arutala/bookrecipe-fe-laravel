<?php

namespace App\Livewire\Form;

use Livewire\Component;

class ResepForm extends Component
{
    public function render()
    {
        return view('livewire.form.resep-form');
    }
}
