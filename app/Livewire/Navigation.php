<?php

namespace App\Livewire;

use Illuminate\Support\Facades\Route;
use Livewire\Component;

class Navigation extends Component
{
    public $isCurrent;

    public function mount()
    {
        $route = Route::currentRouteName();
        $previousRoute = session()->get('navPage') ?? 'daftar-resep';
        if (in_array($route, ['daftar-resep', 'tambah-resep'])) {
            $this->isCurrent = $route;
            session()->put('navPage', $route);
        } else {
            $this->isCurrent = $previousRoute;
        }
    }

    public function render()
    {
        return view('livewire.navigation');
    }
}
