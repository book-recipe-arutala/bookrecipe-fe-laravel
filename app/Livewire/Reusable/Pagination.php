<?php

namespace App\Livewire\Reusable;

use Livewire\Attributes\On;
use Livewire\Component;

class Pagination extends Component
{
    public $pageNumber;
    public $totalPages;

    #[On('changeTotalPage')]
    public function pagination($newPageNumber, $newTotalPages){
        $this->pageNumber = $newPageNumber;
        $this->totalPages = $newTotalPages;
    }

    public function nextPage()
    {
        if ($this->pageNumber < $this->totalPages) {
            $this->pageNumber++;
            $this->changePaginationNumber();
        }
    }

    public function previousPage()
    {
        if ($this->pageNumber > 1) {
            $this->pageNumber--;
            $this->changePaginationNumber();
        }
    }

    public function changePage($page)
    {
        $this->pageNumber = $page;
        $this->changePaginationNumber();
    }


    private function changePaginationNumber()
    {
        $this->dispatch('paginationChanged',  $this->pageNumber);
    }

    public function render()
    {
        return view('livewire.reusable.pagination');
    }
}
