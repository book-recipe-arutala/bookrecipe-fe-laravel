<?php

namespace App\Livewire\Reusable;

use App\Helpers\APIHelper;
use Livewire\Component;

class Filter extends Component
{
    public $levelId;
    public $categoryId;
    public $timeCook;
    public $sortBy;
    public $levelsData;
    public $categoriesData;

    public function mount()
    {
        $this->levelsData = APIHelper::getLevels()['data'];
        $this->categoriesData = APIHelper::getCategories()['data'];
    }

    public function performFilter()
    {
        $this->dispatch(
            'filterPerformed',
            levelId: $this->levelId,
            categoryId: $this->categoryId,
            timeCook: $this->timeCook,
            sortBy: $this->sortBy
        );
    }

    public function render()
    {
        return view('livewire.reusable.filter');
    }
}
