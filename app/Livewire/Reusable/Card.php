<?php

namespace App\Livewire\Reusable;

use App\Helpers\APIHelper;
use Illuminate\Support\Facades\Log;
use Livewire\Attributes\On;
use Livewire\Component;

class Card extends Component
{
    public $data;
    public $showIcon = true;

    public function mount($data)
    {
        $this->data = $data;
    }

    public function showModalDelete()
    {
        Log::info('showModalDispatch');
        $message = 'Apakah Anda yakin akan menghapus resep ' . $this->data['recipeName'];
        $this->dispatch('showModalDelete', message: $message, id: $this->data['recipeId']);
    }

    // #[On('deleteRecipe')]
    // public function delete($id){
    //     if($this->data['recipeId'] == $id){
    //         $response = APIHelper::deleteRecipe($id);
    //         if($response['statusCode'] === 200){
    //             $this->dispatch('deleted', $response['message']);
    //         }
    //         else{
    //             $this->addError('togFavorite', $response['message']);
    //         }
    //     }
    // }

    public function deleteRecipe($id)
    {
        if ($this->data['recipeId'] == $id) {
            $response = APIHelper::deleteRecipe($id);
            $this->js('window.location.reload()');
            if ($response['statusCode'] === 200) {
                $this->dispatch('deleted', $response['message']);
            } else {
                $this->addError('togFavorite', $response['message']);
            }
        }
    }

    #[On('searchPerformed')]
    public function doRefresh()
    {
        $this->dispatch('$refresh');
    }

    public function render()
    {
        return view('livewire.reusable.card');
    }
}
