<?php

namespace App\Livewire\Reusable;

use Livewire\Component;

class ItemsPerPage extends Component
{
    public $selectedEntries;

    public function setEntries($entries){
        $this->selectedEntries = $entries;
        $this->dispatch('entriesUpdated', $this->selectedEntries);
    }

    public function render()
    {
        return view('livewire.reusable.items-per-page');
    }
}
