<?php

namespace App\Livewire\Reusable;

use Livewire\Component;

class ModalInfo extends Component
{
    public function render()
    {
        return view('livewire.reusable.modal-info');
    }
}
