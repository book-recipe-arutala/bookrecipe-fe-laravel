<?php

namespace App\Livewire\Page;

use App\Helpers\APIHelper;
use Illuminate\Support\Facades\Log;
use Livewire\Attributes\On;
use Livewire\Attributes\Title;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Throwable;

#[Title('Edit Resep Masakan - Book Recipe 79')]
class EditResep extends Component
{
    #[Validate]
    public $recipeName;
    #[Validate]
    public $timeCook;
    #[Validate]
    public $selectedCategory;
    #[Validate]
    public $selectedLevel;
    #[Validate]
    public $ingredient;
    #[Validate]
    public $howToCook;
    #[Validate]
    public $image;

    public $categoriesData;
    public $levelsData;

    public $recipeId;
    public $formTittle = 'Edit Resep Masakan';

    public function rules(){
        return [
            'recipeName' => 'required|string|max:255|regex:/^[a-zA-Z\s]+$/',
            'timeCook' => 'required|numeric|max:999|min:1',
            'selectedCategory' => 'required',
            'selectedLevel' => 'required',
            'howToCook' => 'required',
            'ingredient' => 'required',
        ];
    }

    public function messages(){
        return [
            'recipeName.required' => 'Nama Resep Makanan tidak boleh kosong',
            'recipeName.max' => 'Panjang kolom tidak boleh melebihi 255 karakter',
            'recipeName.regex' => 'Kolom tidak boleh berisi special character/angka',

            'timeCook.required' => 'Waktu tidak boleh kosong',
            'timeCook.numeric' => 'Kolom hanya boleh berisi angka 1-999',
            'timeCook.max' => 'Kolom hanya boleh berisi angka 1-999',
            'timeCook.min' => 'Kolom hanya boleh berisi angka 1-999',

            'selectedCategory.required' => 'Kategori Makanan tidak boleh kosong',
            'selectedLevel.required' => 'Tingkat Kesulitan tidak boleh kosong',

            'ingredient.required' => 'Bahan - Bahan tidak boleh kosong',
            'howToCook.required' => 'Bahan - Bahan tidak boleh kosong',
        ];
    }

    public function mount($id){
        $this->fetchData($id);
    }

    public function fetchData($id){
        try {
            $this->recipeId = $id;
            $recipeData = APIHelper::getRecipesDetail($id)['data'];
            $this->categoriesData = APIHelper::getCategories()['data'];
            $this->levelsData = APIHelper::getLevels()['data'];

            $this->recipeName = $recipeData['recipeName'];
            $this->timeCook = $recipeData['timeCook'];
            $this->selectedCategory = $recipeData['categories']['categoryId'];
            $this->selectedLevel = $recipeData['levels']['levelId'];
            $this->image = $recipeData['imageFileName'];
            $this->ingredient = $recipeData['ingredient'];
            $this->howToCook = $recipeData['howToCook'];
        } catch (Throwable $error) {
            Log::error($error->getMessage());
            $this->addError('serverError', 'Terjadi kesalahan server, silahkan coba lagi');
        }
    }

    #[On('ingredientUpdated')]
    public function ingredientUpdated($value){
        if(strip_tags($value) == '' ){
            $this->addError('ingredient', 'Bahan - Bahan tidak boleh kosong');
        }else{
            $this->resetErrorBag('ingredient');
        }
        $this->ingredient = $value;
    }

    #[On('howToCookUpdated')]
    public function howToCookUpdated($value){
        if(strip_tags($value) == ''){
            $this->addError('howToCook', 'Cara memasak tidak boleh kosong');
        }else{
            $this->resetErrorBag('howToCook');
        }
        $this->howToCook = $value;
    }

    public function submit(){
        $data = [
            'recipeName' => $this->recipeName,
            'categories' => [
                'categoryId' => $this->selectedCategory,
            ],
            'levels' => [
                'levelId' => $this->selectedLevel,
            ],
            'imageFilename' => $this->image,
            'timeCook' => $this->timeCook,
            'ingredient' => $this->ingredient,
            'howToCook' => $this->howToCook,
        ];

        $response = APIHelper::updateRecipe($this->recipeId, $data);
        redirect()->route('daftar-resep');
    }

    public function render()
    {
        return view('livewire.form.resep-form');
    }
}
