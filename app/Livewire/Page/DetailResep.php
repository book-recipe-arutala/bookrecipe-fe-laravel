<?php

namespace App\Livewire\Page;

use App\Helpers\APIHelper;
use Livewire\Attributes\Title;
use Livewire\Component;

#[Title('Detail Resep Masakan - Book Recipe 79')]
class DetailResep extends Component
{
    public $id;
    public $data;

    public function mount($id)
    {
        $this->id = $id;
        $this->data = APIHelper::getRecipesDetail($id)['data'];
    }

    public function render()
    {
        return view('livewire.page.detail-resep');
    }
}
