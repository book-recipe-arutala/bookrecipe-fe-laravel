<?php

namespace App\Livewire\Page;

use App\Helpers\APIHelper;
use Illuminate\Support\Facades\Log;
use Livewire\Attributes\On;
use Livewire\Component;

class DaftarResep extends Component
{
    public $recipes;
    public $recipeId;
    public $pageNumber = 1;
    public $pageSize = 8;

    public $totalPages;
    public $totalData;

    public $isLoading = true;

    public $recipeName;
    public $levelId;
    public $categoryId;
    public $timeCook;
    public $sortBy = 'recipeName,asc';

    public function mount()
    {
        $this->recipes = $this->fetchRecipes();
        $this->isLoading = false;
    }

    public function fetchRecipes()
    {
        $this->isLoading = false;
        $response = APIHelper::getRecipes(
            $this->recipeName,
            $this->levelId,
            $this->categoryId,
            $this->timeCook,
            $this->sortBy,
            $this->pageSize,
            $this->pageNumber
        )['data'];

        $this->totalPages = ceil(2 / $this->pageSize);
        $this->isLoading = true;
        return $response;
    }

    #[On('searchPerformed')]
    public function search($search)
    {
        $this->recipeName = $search;
        $this->recipes = $this->fetchRecipes();
    }

    #[On('filterPerformed')]
    public function filter($levelId = null, $categoryId = null, $timeCook = null, $sortBy = null)
    {
        $this->levelId = $levelId;
        $this->categoryId = $categoryId;
        $this->timeCook = $timeCook;
        $this->sortBy = $sortBy;

        $this->recipes = $this->fetchRecipes();
    }

    #[On('entriesUpdated')]
    public function entries($entries){
        $this->pageSize = $entries;
        $this->pageNumber = 1;
        $this->recipes = $this->fetchRecipes();
        $this->dispatch('changeTotalPage', $this->pageNumber, $this->totalPages);
    }

    #[On('paginationChanged')]
    public function pagination($pageNumber){
        $this->pageNumber = $pageNumber;
        $this->recipes = $this->fetchRecipes();
    }

    #[On('choices-delete')]
    public function delete($choices){
        Log::info($choices);
        // $this->flashMessage= null;
        if($choices){
            $this->dispatch('deleteRecipe', id: $this->recipeId);
            // $this->alertId++;
        }
    }

    #[On('showModalDelete')]
    public function showModalDelete($message, $id){
        Log::info('here delete');
        $this->recipeId = $id;

        $this->dispatch(
            'infoAlert-delete-'.$this->alertId,
            message: $message);
    }

    public function render()
    {
        return view('livewire.page.daftar-resep');
    }
}
