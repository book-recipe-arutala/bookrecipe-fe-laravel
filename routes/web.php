<?php

use App\Livewire\Page\DaftarResep;
use App\Livewire\Page\DetailResep;
use App\Livewire\Page\EditResep;
use App\Livewire\Page\TambahResep;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect()->route('daftar-resep');
});

Route::get('/daftar-resep', DaftarResep::class)->name('daftar-resep');
Route::get('/detail-resep/{id}', DetailResep::class)->name('detail-resep');
Route::get('/tambah-resep', TambahResep::class)->name('tambah-resep');
Route::get('/edit-resep/{id}', EditResep::class)->name('edit-resep');
