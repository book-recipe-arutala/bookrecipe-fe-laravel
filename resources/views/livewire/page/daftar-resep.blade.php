<div class="col-12 mb-5">
    <div class="d-none d-sm-flex flex-column align-items-center mt-5">
        <div class="d-flex justify-content-center col-12">
            <a href="{{ route('daftar-resep') }}" class="btn-add btn btn-primary mx-3" role="button">
                <i class="fa-solid fa-plus custom-fontsize-content1"></i> Tambah Resep
            </a>
            <livewire:reusable.search class="custom-fontsize-content1">
            <livewire:reusable.filter class="custom-fontsize-content1">
        </div>
        <h1 class="fw-bold custom-fontsize-subtitle mt-3">Daftar Resep Makanan</h1>
    </div>
    <div class="d-flex justify-content-center w-100">
        <div class="row parent-card justify-content-start mt-3">
            @foreach ($recipes as $data)
                <livewire:reusable.card :data="$data" :key="$data['recipeId']">
            @endforeach
            <div class="mx-md-2">
                <div class="d-md-flex align-items-center justify-content-between mt-4 mx-auto">
                    <livewire:reusable.items-per-page :selectedEntries="$pageSize">
                    <livewire:reusable.pagination :totalPages="$totalPages" :pageNumber="$pageNumber">
                </div>
            </div>
        </div>
    </div>
</div>
