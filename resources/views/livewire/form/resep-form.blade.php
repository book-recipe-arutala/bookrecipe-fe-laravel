<div class="mx-auto p-4">
    <form wire:submit.prevent="submit" class="form-recipe">
        <h2 class="text-center fw-bolder mb-5 mt-3">{{ $formTittle }}</h2>
        <div class="row mx-auto my-2 justify-content-center">
            <div class="col-md-5 mx-2">
                <div>
                    <label for="recipeName" class="fs-6">Nama Resep Masakan<strong
                            class="text-danger ">*</strong></label>
                    <input id="recipeName" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                        type="text" wire:model.live="recipeName" placeholder="Nama Resep Masakan">
                    @error('recipeName')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <label for="image" class="fs-6">Gambar Makanan (URL)*<strong
                            class="text-danger ">*</strong></label>
                    <input id="image" class="form-control mt-2 @error('image') border-danger @enderror"
                        type="text" wire:model.live="image" placeholder="Gambar Masakan (URL)">
                    @error('image')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4 fs-6">
                    <label>Bahan-bahan<strong class="text-danger ">*</strong></label>
                    <div class="@error('howToCook') rich-text-danger @enderror">
                        <livewire:reusable.rich-text name="ingredient" value="{!! $ingredient !!}" />
                    </div>
                    @error('howToCook')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col-md-5 mx-2">
                <div>
                    <label for="category" class="fs-6">Kategori Masakan<strong class="text-danger ">*</strong></label>
                    <select id="category" wire:model="selectedCategory"
                        class="form-select mt-2 @error('selectedCategory') border-danger @enderror" placeholder>
                        <option value=null selected disabled class="text-center">Pilih Kategori</option>
                        @foreach ($categoriesData as $category)
                            <option value="{{ $category['categoryId'] }}">
                                {{ $category['categoryName'] }}
                            </option>
                        @endforeach
                    </select>
                    @error('selectedCategory')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <label for="time" class="fs-6">Waktu Memasak (Menit)<strong
                                class="text-danger ">*</strong></label>
                        <input id="time" class="form-control mt-2 @error('timeCook') border-danger @enderror"
                            type="number" wire:model.live="timeCook" placeholder="Waktu Memasak">
                        @error('timeCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="level" class="fs-6">
                            Tingkat Kesulitan<strong class="text-danger ">*</strong>
                        </label>
                        <select id="level" wire:model="selectedLevel"
                            class="form-select mt-2 @error('selectedLevel') border-danger @enderror">
                            <option value=null selected disabled>Pilih Tingkat Kesulitan</option>
                            @foreach ($levelsData as $level)
                                <option value="{{ $level['levelId'] }}">{{ $level['levelName'] }}</option>
                            @endforeach
                        </select>
                        @error('selectedLevel')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="mt-4 fs-6">
                    <label>Cara Memasak<strong class="text-danger ">*</strong></label>
                    <div class="@error('howToCook') rich-text-danger @enderror">
                        <livewire:reusable.rich-text name="howToCook" value="{!! $howToCook !!}" />
                    </div>
                    @error('howToCook')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="d-flex justify-content-end my-3 buttons-container">
                    <button class="btn btn-recipe-outline-primary mb-md-0 me-2" wire:click='cancel'>Batal</button>
                    <button type="submit" class="btn btn-recipe-primary">Submit</button>
                </div>

            </div>
        </div>
    </form>
</div>
