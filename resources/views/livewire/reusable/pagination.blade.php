<div class="pagination-container">
    <button class="pagination-button" wire:click="previousPage" @if ($pageNumber == 1) disabled @endif>
        <div class=""><i class="ri-arrow-left-s-line"></i></div>
    </button>
    @foreach (range(1, $totalPages) as $page)
        <button
            class="btn pagination-button custom-fontsize-content2 {{ $page == $pageNumber ? 'selected-pagination' : '' }}"
            wire:click="changePage({{ $page }})" @if ($page == $pageNumber) disabled @endif>
            <div class="">{{ $page }}</div>
        </button>
    @endforeach
    <button class="pagination-button" wire:click="nextPage" @if ($pageNumber == $totalPages) disabled @endif>
        <div class=""><i class="ri-arrow-right-s-line"></i></div>
    </button>
</div>
