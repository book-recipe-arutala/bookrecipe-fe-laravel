<div class="col-lg-3 col-md-4 col-sm-6 mb-3">
    <!-- Modal -->
    <div class="modal fade" id="modalDeleteRecipe{{ $data['recipeId'] }}" tabindex="-1"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end border-0" >
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex flex-column mx-auto w-max-xs">
                    <i class="text-recipe-warning mx-auto ri-9x ri-error-warning-line"></i>
                    <p class="text-center mx-md-5 mx-2 text-break fs-4 custom-fontsize-content2">
                        Apakah anda yakin ingin menghapus resep
                        {{ $data['recipeName'] }}?
                    </p>
                    <div class="py-md-4 py-3 mx-auto d-flex w-max-xs">
                        <button class="mx-md-5 mx-1 roaunded btn btn-lg btn-recipe-outline-primary choices" data-bs-dismiss="modal">Tidak</button>
                        <button wire:click='deleteRecipe({{ $data['recipeId'] }})' class="mx-md-5 mx-1 rounded btn btn-lg btn-recipe-primary choices" data-bs-dismiss="modal">Ya</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col d-flex justify-content-center align-items-center w-100">
        <div class="justify-content-center">
            <div class="card">
                <div class="btn-group">
                    <a href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-ellipsis" style="color: white;"></i>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <li>
                            <a class="dropdown-item text-recipe-primary"
                                href="{{ route('edit-resep', ['id' => $data['recipeId']]) }}">
                                <i class="fa-solid fa-pen-to-square"></i> Edit</a>
                        </li>
                        <!-- Button trigger modal -->
                        {{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#modalDelete">
                            Launch demo modal
                        </button> --}}
                        <li>
                            {{-- <a class="dropdown-item text-recipe-danger" wire:click='showModalDelete'>
                                <i class="fa-solid fa-trash"></i> Hapus</a> --}}
                            <a class="dropdown-item text-recipe-danger" data-bs-toggle="modal" data-bs-target="#modalDeleteRecipe{{ $data['recipeId'] }}">
                                <i class="fa-solid fa-trash"></i> Hapus</a>
                        </li>
                    </ul>
                </div>
                <div class="image-container d-flex align-items-center justify-content-center overflow-y-hidden">
                    <img src="{{ $data['imageFileName'] }}" alt="{{ $data['recipeName'] }}" class="img-fluid">
                </div>

                <div class="details">
                    <div class="d-flex justify-content-between mt-2">
                        <p>{{ $data['categories']['categoryName'] }}</p>
                        <p>{{ $data['levels']['levelName'] }}</p>
                    </div>
                    <p class="p-recipe fw-bold">{{ $data['recipeName'] }}</p>

                    <div class="mt-0">
                        <div class="d-flex align-items-center mb-0">
                            <div class="d-flex align-items-center">
                                <i class="fa-regular fa-clock"></i>
                                <p class="mb-0 mx-2">{{ $data['timeCook'] }} Menit</p>
                            </div>
                            <div class="d-flex align-items-center ms-auto">
                            </div>
                        </div>
                        <div class="mt-3 mb-2 w-100 d-flex justify-content-center">
                            <a href="{{ route('detail-resep', ['id' => $data['recipeId']]) }}"
                                class="link-recipe-primary link-underline-info">Lihat detail resep</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
