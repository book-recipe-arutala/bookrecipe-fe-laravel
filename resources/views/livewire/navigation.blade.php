<div class="bg-recipe-primary px-2">
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <a href="{{ route('tambah-resep') }}" class="navbar-brand d-flex align-self-center">
                <img src="{{ asset('icon/logo.svg') }}" alt="Book-recipe" style="max-width: 40px;"
                    class="d-inline-block align-text-top img-fluid">
                <h4 class="my-2 mx-2 text-light fw-bold custom-fontsize-title">Buku Resep</h4>
            </a>
            <div class="d-flex flex-row justify-content-center align-items-center">
                <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                    <i class="ri-menu-line"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <a href="{{ route('daftar-resep') }}"
                        class="link-underline link-underline-opacity-0 mx-4 fs-6 fw-bold {{ Route::is('daftar-resep') ? 'link-recipe-primary' : 'link-light' }} ">
                        Daftar Resep Masakan
                    </a>
                    <a href="{{ route('tambah-resep') }}"
                        class="link-underline link-underline-opacity-0 mx-3 fs-6 fw-bold {{ Route::is('tambah-resep') ? 'link-recipe-primary' : 'link-light' }}">
                        Tambah Resep
                    </a>
                </div>
            </div>
        </div>
    </nav>

    <!-- Offcanvas/Sidebar -->
    <nav class="navbar">
        <div class="offcanvas offcanvas-end bg-recipe-primary-offcanvas" tabindex="-1" id="offcanvasExample"
            aria-labelledby="offcanvasExampleLabel">
            <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            <a href="{{ route('daftar-resep') }}"
                class="link-underline link-underline-opacity-0 mx-4 fs-6 fw-bold {{ $isCurrent == 'daftar-resep' ? 'link-recipe-primary' : 'link-light' }} ">
                Daftar Resep Masakan
            </a>
            <a href="{{ route('tambah-resep') }}"
                class="link-underline link-underline-opacity-0 mx-3 fs-6 fw-bold {{ $isCurrent == 'my-recipes' ? 'link-recipe-primary' : 'link-light' }}">
                Tambah Resep
            </a>
        </div>
    </nav>
</div>
